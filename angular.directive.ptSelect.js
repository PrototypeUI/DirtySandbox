angular
  .module('pt:select', [])
  .directive('ptSelect', function($compile) {
    return {

      restrict: 'A',
      require: ['select'],
      link: function($scope, $el, $attrs, controller) {

        $el.addClass('pt-select')
        $el.after('<div class="pt-select" pt-block="auto" pt-component="select"><ul></ul></div>')

        // this will ensure that anything like pt-block in the template code gets compiled.
        $compile($el.next())($scope.$new());


        var $output = $el.next().is('div.pt-block') ? $el.next().find('div.pt-select') : $el.next(),
          $list = $output.find('ul'),
          $dropdown = $el;


        var rebuildList = function() {
          $dropdown.find('option').each(function() {
            var $option = $("<li>" + $(this).text() + "</li>").data('value', $(this).val());

            if (this.selected) {
              $option.addClass('selected')
            }
            $list.append($option)
          })
        };
        rebuildList();
        $scope.$watch(function() {
          $list.empty();
          rebuildList();
        })


        // this indicates that the pt-block has changed
        $scope.$on('pt:layout#select', function(e, $blockScope, $context, $el) {


          // if the element is less than 30em, and previously was not less than 30em
          if (($context['max-width'].indexOf('30em') !== -1 && $context['layout'].indexOf('column') !== -1) || $context['layout'].indexOf('text-row-long') !== -1) {

            $dropdown.off('.ptSelect');
            $output.off('.ptSelect');


            $dropdown.on('change.ptSelect', function() {
              var $val = $(this).val();

              $list.find('li').removeClass('selected').filter(function() {
                return $(this).data("value") == $val;
              }).addClass('selected');
            })
            $output.on('click.ptSelect', function(e) {
              var $target = $(e.target);
              if ($target.is("li")) {
                $list.find('li.selected').removeClass('selected')
                $target.addClass('selected')
                $dropdown.val($target.data('value')).change();
                $output.removeClass('active')
              }
              e.stopPropagation();
              return false;
            });

          } else {

            $dropdown.off('.ptSelect');
            $output.off('.ptSelect');

            $dropdown.on('focus.ptSelect', function() {
              $output.trigger('click')
            })
            $dropdown.on('blur.ptSelect', function() {
              $output.trigger('click')
            })
            $dropdown.on('change.ptSelect', function() {
              var $val = $(this).val();

              $list.find('li').removeClass('selected').filter(function() {
                return $(this).data("value") == $val;
              }).addClass('selected');
            })
            $output.on('click.ptSelect', function(e) {
              var $target = $(e.target);
              if (!$output.is('.active')) {
                $output.addClass('active')
              } else if ($target.is("li")) {
                $list.find('li.selected').removeClass('selected')
                $target.addClass('selected')
                $dropdown.val($target.data('value')).change();
                $output.removeClass('active');
              } else {
                $output.removeClass('active');
              }
              e.stopPropagation();
              return false;
            });


          }
        });
      }

    };
  });