# Prototype Experimentation Sandbox

Nothing in this repository is production ready. Specifications and design decisions could change drastically. Let's use this for collaboration and discussion to work through the core UI framework challenges and optimizations.

## What is Prototype
__An idea.__ The idea is to create a library of re-usable, minimalistic, context-aware components that can be easily used to create  a beautiful and functional prototype for any web application. ie.
_"The minimalist, context-aware adaptive HTML5 web application UI Kit."_

## The Plan
This idea is still in it's infant stages, but here's the general plan. Read more at https://prototype.hackpad.com/Prototype-z9jIx5lAfLA

* Strong focus on extreme minimalism.
 * any design opinion should be in theming layers
 * minimal themes should be well documented
 * there should be some sort of global settings that compile into production code (compass, stylus)
 * focus on layout more than look at the core style
* Make it easy to extend and override anything.
 * document the api events that directives create
 * explain what listeners are added and how to remove them or change them
 * provide examples of override
 * block even the core javascript ready scripts if a user wants to override them
* Make it easy to use any directive independently of the entire framework
 * precompile CSS
 * deploy as component.io packages and bower packages
* Integrate with existing development workflow tools
 * integrate with `yo angular` in a way that allows for proper unit tests etc
 * use grunt for compiling and testing components

## Goals of this repo

* Decide on whether or not the attribute selector should be used for CSS context breakpoints
* Optimize the `ptBlock` directive. There may be issues with the scoping breaking stuff for people. See https://github.com/angular/angular.js/wiki/Understanding-Scopes
* Work on the `ptSelect` directive a bit more and try to find issues with the implementation.
* Expand the directives to allow for configuration choices (and figure out how that impacts event `$emit`ing). (possibly need an isolate scope for this?)
* Add a few more directives in completely different categories to try and find more bugs.

