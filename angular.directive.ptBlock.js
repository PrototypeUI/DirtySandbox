angular
  .module('pt:block', [])
  .directive('ptBlock', function() {
    return {
      restrict: 'A',
      scope: true,
      controller: function($scope) {

        var _initBlock = function($el) {


          // @todo cleanup
          var emConvertor = $("#em").width();
          var windowWidth = $(window).width() / emConvertor;

          // @todo integrate with root scope
          var maxWidth = 100,
            maxHeight = 70,
            emWidth = $el.width() / emConvertor,
            emHeight = $el.height() / emConvertor;


          // make sure we don't really care about heights or widths out of range.
          emWidth = emWidth > maxWidth ? maxWidth : emWidth;
          emHeight = emHeight > maxHeight ? maxHeight : emHeight;

          var width = {
            max: [],
            min: []
          },
            height = {
              max: [],
              min: []
            };

          var i = 1;
          while (i <= maxWidth) {
            if (i <= emWidth)
              width.min.push(i + 'em')
            if (i >= emWidth)
              width.max.push(i + 'em')

            // make sure our iterators are jumping correctly
            if (i < 5) i++;
            else if (i == 5) i += 5;
            else if (i >= 10) i += 10;
          }

          var i = 1;
          while (i <= maxHeight) {
            if (i <= emHeight)
              height.min.push(i + 'em')
            if (i >= emHeight)
              height.max.push(i + 'em')

            // make sure our iterators are jumping correctly
            if (i < 5) i++;
            else if (i == 5) i += 5;
            else if (i >= 10) i += 10;
          }

          var layout = [];

          // set landscape or portrait
          if (emWidth > emHeight)
            layout.push('landscape');
          else
            layout.push('portrait');

          // if it's within common widescreen, let's make it picture
          if (emWidth / emHeight <= 1.85 && emHeight / emWidth <= 1.85)
            layout.push('picture');

          // row has to be at least 4x as long as it's high
          if (emWidth / emHeight >= 4) {
            layout.push('row');

            // of the height is less than 4em (4 lines of text high) it's only good for one text row (good for detecting single line-spaces)
            if ($el.is('.pt-fixed-view') && emHeight <= 4 && emHeight >= 1) {
              layout.push('text-row');

              // if it's greater than 30em it's sufficient for a long row of text
              if (emWidth >= 30)
                layout.push('text-row-long');

              // otherwise you're limited to just a few words
              if (emWidth < 30)
                layout.push('text-row-short');
            }
          }

          // if the proportions are square or taller than square let's call it a column
          if (emWidth / emHeight <= 1)
            layout.push('column');

          var response = {
            'layout': layout,
            'min-width': width.min,
            'max-width': width.max,
            'min-height': height.min,
            'max-height': height.max
          };
          // now let's set the attributes correctly
          $el.attr({
            'layout': layout.join(' '),
            'min-width': width.min.join(' '),
            'max-width': width.max.join(' '),
            'min-height': height.min.join(' '),
            'max-height': height.max.join(' ')
          });

          return response;
        };
        // wait for a layout change event
        $scope.$on('pt:redraw', function(e) {
          var currentData = _initBlock($scope.$ptBlock);
          //if(!angular.equals(currentData,$scope.$ptContext)) {
          $scope.$ptContextPrev = $scope.$ptContext;
          $scope.$ptContext = currentData;
          if ($scope.$ptComponentName) {
            $scope.$emit('pt:layout#' + $scope.$ptComponentName, $scope, $scope.$ptContext, $scope.$ptBlock);
          }
          //}

          // evaluate configured breakpoints and callbacks

          // emit an event upstream that allows for parent controllers to customize behavior on layout changes

        });

      },
      link: function($scope, $el, $attr) {

        function $initBlock(element) {
          element.addClass('pt-block');
          $scope.$ptContext = null;
          $scope.$ptContextPrev = null;
          $scope.$ptComponentName = typeof $el.attr('pt-component') == 'undefined' ? false : $el.attr('pt-component');
          $scope.$ptBlock = element;
          $scope.$broadcast('pt:redraw');
          $(window).off('resize.pt-redraw').on('resize.pt-redraw', function() {
            $scope.$root.$broadcast('pt:redraw');
          });
        }


        // Turn this element into a protototype block if pt-block="view" or pt-block is empty and $el has a class name of .pt-block
        if ($attr.ptBlock == 'view' || ($attr.ptBlock === '' && $el.is('.pt-block'))) {
          return $initBlock($el);
        }

        // #inherit requires that this element is placed in a pt-block
        // #auto will check to see if there's a parent element, otherwise it'll wrap itself
        if ($attr.ptBlock == 'inherit' || $attr.ptBlock == 'auto') {
          if ($el.parent().attr('pt-block') == 'view') {

            $scope.$ptContext = null;
            $scope.$ptContextPrev = null;
            $scope.$ptComponentName = typeof $el.attr('pt-component') == 'undefined' ? false : $el.attr('pt-component');
            $scope.$ptBlock = $el.parent();
            return;
          } else if ($attr.ptBlock == 'inherit') {
            throw "You can only use pt-block='inherit' when your element is a direct child of a pt-block='view'.";
          }
        }

        // otherwise the type is "wrap" and we'll go ahead and wrap this element.
        $el.wrap('<div class="pt-block"></div>');
        $initBlock($el.parent());
      }
    };
  });